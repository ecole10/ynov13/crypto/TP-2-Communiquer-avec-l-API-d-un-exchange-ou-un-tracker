package com.crypto.tracker.controller;

import com.crypto.tracker.service.CryptoService;
import com.fasterxml.jackson.databind.JsonNode;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "Crypto")
@RestController
@RequestMapping("/api/cryptos")
@RequiredArgsConstructor
public class CryptoController {
    private final CryptoService cryptoService;

    @GetMapping("/info")
    @Operation(summary = "Get infos for specific crypto", responses = {
            @ApiResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = JsonNode.class))),
    })
    public ResponseEntity<JsonNode> getInfos(@RequestParam String symbol) {
        return new ResponseEntity<>(cryptoService.getInfos(symbol), OK);
    }

    @GetMapping("/prices")
    @Operation(summary = "Get prices for specific crypto", responses = {
            @ApiResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = JsonNode.class))),
    })
    public ResponseEntity<JsonNode> getPrices(@RequestParam String symbol, @RequestParam(defaultValue = "USD") String currency) {
        return new ResponseEntity<>(cryptoService.getPrices(symbol, currency), OK);
    }
}
