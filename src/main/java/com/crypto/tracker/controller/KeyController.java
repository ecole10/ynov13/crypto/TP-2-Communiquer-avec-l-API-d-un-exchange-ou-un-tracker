package com.crypto.tracker.controller;

import com.crypto.tracker.enumeration.Platform;
import com.crypto.tracker.service.KeyService;
import com.fasterxml.jackson.databind.JsonNode;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Tag(name = "Key")
@RestController
@RequestMapping("/api/keys")
@RequiredArgsConstructor
public class KeyController {
    private final KeyService keyService;

    @GetMapping("/info")
    @Operation(summary = "Get key info on specific platform", responses = {
        @ApiResponse(responseCode = "200", content = @Content(mediaType = APPLICATION_JSON_VALUE, schema = @Schema(implementation = JsonNode.class))),
    })
    public ResponseEntity<JsonNode> getKeyInfo(@RequestParam Platform platform) {
        return new ResponseEntity<>(keyService.getKeyInfo(platform), OK);
    }
}
