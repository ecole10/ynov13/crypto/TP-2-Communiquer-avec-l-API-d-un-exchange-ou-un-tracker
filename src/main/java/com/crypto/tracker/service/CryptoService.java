package com.crypto.tracker.service;

import com.crypto.tracker.webservice.coinmarketcap.api.cryptocurrency.CoinMarketCapGetCryptocurrencyInfo;
import com.crypto.tracker.webservice.coinmarketcap.api.cryptocurrency.CoinMarketCapGetCryptocurrencyQuotesLatest;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CryptoService {
    private final CoinMarketCapGetCryptocurrencyInfo coinMarketCapGetCryptocurrencyInfo;
    private final CoinMarketCapGetCryptocurrencyQuotesLatest coinMarketCapGetCryptocurrencyQuotesLatest;

    public JsonNode getInfos(String symbol) {
        JsonNode response = this.coinMarketCapGetCryptocurrencyInfo.send(symbol);
        return response.get("data");
    }
    public JsonNode getPrices(String symbol, String currency) {
        JsonNode response = this.coinMarketCapGetCryptocurrencyQuotesLatest.send(symbol, currency);
        return response.get("data").get(symbol).get("quote").get(currency);
    }
}
