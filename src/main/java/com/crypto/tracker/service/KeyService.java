package com.crypto.tracker.service;

import com.crypto.tracker.enumeration.Platform;
import com.crypto.tracker.webservice.coinmarketcap.api.key.CoinMarketCapGetKeyInfo;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class KeyService {
    private final CoinMarketCapGetKeyInfo coinMarketCapGetKeyInfo;

    public JsonNode getKeyInfo(Platform platform) {
        JsonNode response = switch (platform) {
            case COINMARKETCAP -> this.coinMarketCapGetKeyInfo.send();
        };

        return response.get("data");
    }
}
