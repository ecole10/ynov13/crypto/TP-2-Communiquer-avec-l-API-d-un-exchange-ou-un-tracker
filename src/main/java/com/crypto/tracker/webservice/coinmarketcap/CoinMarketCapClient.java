package com.crypto.tracker.webservice.coinmarketcap;

import com.crypto.tracker.webservice.AbstractClient;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import static io.netty.handler.codec.http.HttpScheme.HTTPS;
import static org.springframework.http.HttpHeaders.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * @see <a href="https://coinmarketcap.com/api/documentation/v1/">CoinMarketCap API docs</a>
 */
@Service
public class CoinMarketCapClient extends AbstractClient {
    private final static String BASE_URL = "pro-api.coinmarketcap.com";
    private String apiKey;

    public CoinMarketCapClient(@Value("${coinmarketcap.api.key}") String apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public JsonNode get(String uri) {
        return webClient.get()
            .uri(uriBuilder -> uriBuilder
                .scheme(HTTPS.name().toString())
                .host(BASE_URL)
                .path(uri)
                .build())
            .header("X-CMC_PRO_API_KEY", this.apiKey)
            .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
            .header(ACCEPT, APPLICATION_JSON_VALUE)
            .retrieve()
            .bodyToMono(JsonNode.class)
            .block();
    }

    @Override
    public JsonNode get(String uri, MultiValueMap<String, String> params) {
        return webClient.get()
            .uri(uriBuilder -> uriBuilder
                .scheme(HTTPS.name().toString())
                .host(BASE_URL)
                .path(uri)
                .queryParams(params)
                .build())
            .header("X-CMC_PRO_API_KEY", this.apiKey)
            .header(CONTENT_TYPE, APPLICATION_JSON_VALUE)
            .header(ACCEPT, APPLICATION_JSON_VALUE)
            .retrieve()
            .bodyToMono(JsonNode.class)
            .block();
    }
}
