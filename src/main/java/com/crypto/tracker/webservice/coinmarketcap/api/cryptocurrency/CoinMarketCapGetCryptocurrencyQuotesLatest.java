package com.crypto.tracker.webservice.coinmarketcap.api.cryptocurrency;

import com.crypto.tracker.webservice.coinmarketcap.CoinMarketCapClient;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

@Service
@RequiredArgsConstructor
public class CoinMarketCapGetCryptocurrencyQuotesLatest {
    private final CoinMarketCapClient coinMarketCapClient;

    public JsonNode send(String symbol, String currency) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("symbol", symbol);
        params.add("convert", currency);

        return coinMarketCapClient.get("/v1/cryptocurrency/quotes/latest", params);
    }
}
