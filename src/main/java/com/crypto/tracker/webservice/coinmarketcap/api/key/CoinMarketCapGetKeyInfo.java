package com.crypto.tracker.webservice.coinmarketcap.api.key;

import com.crypto.tracker.webservice.coinmarketcap.CoinMarketCapClient;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CoinMarketCapGetKeyInfo {
    private final CoinMarketCapClient coinMarketCapClient;

    public JsonNode send() {
        return coinMarketCapClient.get("/v1/key/info");
    }
}
